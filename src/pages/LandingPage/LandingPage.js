import React from 'react';

import styled from 'styled-components';

import { Button, Grid } from '@material-ui/core';

import StandardLayout from '../../layouts/StandardLayout/StandardLayout';
import MovieCard from '../../components/MovieCard';
import MovieList from '../../components/MovieList';

const Buttons = styled.div`
  margin: 0 auto;
  margin-bottom: 3rem;
  text-align: center;

  & > *:not(:last-child) {
    margin-right: 2rem;
  }
`;

const LandingPage = () => {
  const dummyArray = [1, 2, 3, 4, 5, 6];
  return (
    <div>
      <StandardLayout>
        <Grid item>
          <Buttons>
            <Button variant="contained" color="secondary">Latest</Button>
            <Button variant="contained" color="secondary">Top Rated</Button>
            <Button variant="contained" color="secondary">Popular</Button>
            <Button variant="contained" color="secondary">Analytics</Button>
          </Buttons>
        </Grid>
        <MovieList>
          <Grid container spacing={4}>
            {dummyArray.map(() => (
              <Grid item xs={12} sm={4}>
                <MovieCard />
              </Grid>
            ))}
          </Grid>
        </MovieList>
      </StandardLayout>
    </div>
  );
};

export default LandingPage;
