import React from 'react';

import styled from 'styled-components';

import { Grid, Typography } from '@material-ui/core';

import StandardLayout from '../../layouts/StandardLayout/StandardLayout';
import MovieCard from '../../components/MovieCard';
import MovieDetail from '../../components/MovieDetail';
import PersonList from '../../components/PersonList';
import Review from '../../components/Review';

const StyledGrid = styled(Grid)`
  margin-bottom: 3rem;
`;

const Img = styled.img`
  display: inline-block;
  margin-right: 1rem;
  width: 100px;
`;

const MoviePage = () => {
  const similarMoviesArray = [1, 2, 3, 4];
  const reviewList = [1, 2, 3];
  return (
    <div>
      <StandardLayout>
        <StyledGrid container spacing={4}>
          <Grid item xs={12} sm={4}>
            <MovieCard />
          </Grid>
          <Grid item xs={12} sm={4}>
            <MovieDetail />
          </Grid>
          <Grid item xs={12} sm={4}>
            <Typography variant="h6">Similar movies</Typography>
            <Grid item container>
              {similarMoviesArray.map(() => (
                <Grid item>
                  <Img
                    src="https://img.yts.mx/assets/images/movies/The_Lord_of_the_Rings_The_Fellowship_of_the_Ring_EXTENDED_2001/medium-cover.jpg"
                    alt="Similar movie"
                  />
                </Grid>
              ))}
            </Grid>
          </Grid>
        </StyledGrid>
        <StyledGrid container>
          <Grid item xs={12} sm={4}>
            <iframe
              width="1239"
              height="697"
              src="https://www.youtube.com/embed/r5X-hFf6Bwo"
              title="YouTube video player"
              frameBorder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen
            />
          </Grid>
        </StyledGrid>
        <StyledGrid container spacing={4}>
          <Grid item xs={12} sm={6}>
            {reviewList.map(() => <Review />)}
          </Grid>
          <Grid item xs={12} sm={6}>
            <PersonList />
          </Grid>
        </StyledGrid>
      </StandardLayout>
    </div>
  );
};

export default MoviePage;
