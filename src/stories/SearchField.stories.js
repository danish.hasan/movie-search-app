import React from 'react';
import PropTypes from 'prop-types';

import { TextField } from '@material-ui/core';

export default {
  title: 'Form/SearchField',
  component: TextField,
};

const Template = ({ color, variant, label }) => (
  <TextField label={label} type="search" variant={variant} color={color} />
);

Template.propTypes = {
  color: PropTypes.string,
  variant: PropTypes.string,
  label: PropTypes.string,
};

Template.defaultProps = {
  color: 'primary',
  variant: 'outlined',
  label: 'Search movies',
};

export const SearchField = Template.bind({});

SearchField.args = {
  label: 'Search movies',
  color: 'default',
  variant: 'outlined',
};
