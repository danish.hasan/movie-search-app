import React from 'react';
import PropTypes from 'prop-types';

import ButtonMui from '@material-ui/core/Button';

export default {
  title: 'Form/Button',
  component: ButtonMui,
  argTypes: {
    onClick: {
      action: 'clicked',
    },
    color: {
      options: ['default', 'primary', 'secondary', 'inherit'],
    },
  },
};

const Template = ({
  color,
  children,
  variant,
  onClick,
}) => (
  <ButtonMui color={color} variant={variant} onClick={onClick}>{children}</ButtonMui>
);

Template.propTypes = {
  color: PropTypes.string,
  children: PropTypes.node,
  variant: PropTypes.string,
  onClick: PropTypes.func,
};

Template.defaultProps = {
  color: 'primary',
  children: 'Button',
  variant: 'contained',
  onClick: () => alert('Clicked!'),
};

export const Button = Template.bind({});

Button.args = {
  variant: 'outlined',
  color: 'primary',
  children: 'Button',
};
