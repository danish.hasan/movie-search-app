import React from 'react';

import MyFooter from '../components/Footer';

export default {
  title: 'Footer',
  component: MyFooter,
};

export const Footer = () => <MyFooter />;
