import React from 'react';

import MyModal from '../components/Modal';

export default {
  title: 'Modal',
  component: MyModal,
};

export const Modal = () => <MyModal label="Open Modal">Add content here</MyModal>;
