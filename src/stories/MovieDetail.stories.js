import React from 'react';

import MyMovieDetail from '../components/MovieDetail';

export default {
  title: 'MovieDetail',
  component: MyMovieDetail,
};

export const MovieDetail = () => <MyMovieDetail />;
