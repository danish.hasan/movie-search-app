import React from 'react';

import { Grid } from '@material-ui/core';

import MyMovieList from '../components/MovieList';

import MovieCard from '../components/MovieCard';

export default {
  title: 'MovieList',
  component: MyMovieList,
};

export const MovieList = () => {
  const dummyArray = [1, 2, 3, 4, 5, 6];

  return (
    <MyMovieList>
      <Grid container spacing={4}>
        {dummyArray.map(() => (
          <Grid item xs={12} sm={4}>
            <MovieCard />
          </Grid>
        ))}
      </Grid>
    </MyMovieList>
  );
};
