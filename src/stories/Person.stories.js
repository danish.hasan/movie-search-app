import React from 'react';
import PropTypes from 'prop-types';

import MyPerson from '../components/Person';

export default {
  title: 'Person',
  component: MyPerson,
};

const Template = ({ name, pic }) => (
  <MyPerson name={name} pic={pic} />
);

Template.propTypes = {
  name: PropTypes.string,
  pic: PropTypes.string,
};

Template.defaultProps = {
  name: 'John Doe',
  pic: 'https://img.yts.mx/assets/images/actors/thumb/nm0001392.jpg',
};

export const Person = Template.bind({});

Person.args = {
  name: 'John Doe',
  pic: 'https://t3.ftcdn.net/jpg/03/46/83/96/360_F_346839683_6nAPzbhpSkIpb8pmAwufkC7c5eD7wYws.jpg',
};
