import React from 'react';

import MyHeader from '../components/Header';

export default {
  title: 'Header',
  component: MyHeader,
};

export const Header = () => <MyHeader />;
