import React from 'react';

import MyReview from '../components/Review';

export default {
  title: 'Review',
  component: MyReview,
};

export const Review = () => <MyReview />;
