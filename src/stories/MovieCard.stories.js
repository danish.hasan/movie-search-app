import React from 'react';

import MyMovieCard from '../components/MovieCard';

export default {
  title: 'MovieCard',
  component: MyMovieCard,
};

export const MovieCard = () => <MyMovieCard />;
