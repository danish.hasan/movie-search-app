import React from 'react';

import { createGlobalStyle } from 'styled-components';

import { ThemeProvider } from '@material-ui/styles';

import MoviePage from './pages/MoviePage';
import theme from './theme';

const GlobalStyles = createGlobalStyle`
  * {
    font-family: Roboto;
  }
`;

const App = () => (
  <ThemeProvider theme={theme}>
    <GlobalStyles />
    <MoviePage />
  </ThemeProvider>
);

export default App;
