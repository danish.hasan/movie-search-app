const MUI_PRIMARY = '#052f5f';
const MUI_SECONDARY = '#D5C67A';

export {
  MUI_PRIMARY,
  MUI_SECONDARY,
};

export default {
  MUI_PRIMARY,
  MUI_SECONDARY,
};
