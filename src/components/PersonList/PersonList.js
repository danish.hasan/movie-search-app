import React from 'react';

import styled from 'styled-components';

import { Typography } from '@material-ui/core';

import Person from '../Person';

const StyledPersonList = styled.div`
  background-color: #ddd;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 1.5rem;
`;

const CastSection = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const Cast = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
`;

const PersonList = () => {
  const dummyArray = [1, 2, 3, 4, 5, 6];
  return (
    <StyledPersonList>
      <div>
        <Typography variant="h6">Director</Typography>
        <Person name="Peter Jackson" pic="https://img.yts.mx/assets/images/actors/thumb/nm0001392.jpg" />
      </div>
      <CastSection>
        <Typography variant="h6">Cast</Typography>
        <Cast>
          {dummyArray.map(() => <Person name="Peter Jackson" pic="https://img.yts.mx/assets/images/actors/thumb/nm0001392.jpg" />)}
        </Cast>
      </CastSection>
    </StyledPersonList>
  );
};

export default PersonList;
