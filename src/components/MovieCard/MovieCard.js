import React, { useState } from 'react';

import styled from 'styled-components';

import { IconButton, Typography } from '@material-ui/core/';
import { Favorite, FavoriteBorder } from '@material-ui/icons/';

import { MUI_SECONDARY } from '../../utils/constants/colors';

const StyledMovieCard = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  width: 210px;
`;

const MovieCardPictureOverlay = styled.div`
  background-color: rgba(0, 0, 0, 0.7);
  bottom: 100%;
  height: 0;
  left: 0;
  overflow: hidden;
  position: absolute;
  right: 0;
  transition: .5s ease;
  width: 100%;
`;

const MovieCardPicture = styled.div`
  border: 5px solid black;
  height: 315px;
  margin: 0 auto;
  position: relative;
  transition: all 0.2s;
  width: 100%;

  &:hover {
    border-color: ${MUI_SECONDARY};
  }

  &:hover ${MovieCardPictureOverlay} {
    bottom: 0;
    height: 100%;
  }

  & img {
    display: block;
    height: 100%;
    object-fit: cover;
    position: relative;
    width: 100%;
  }
`;

const MovieCardPictureOverlayText = styled.div`
  color: white;
  font-size: 20px;
  left: 50%;
  overflow: hidden;
  position: absolute;
  text-align: center;
  top: 50%;
  transform: translate(-50%, -50%);
  white-space: nowrap; 
`;

const MovieCard = () => {
  const [isLiked, setIsLiked] = useState(false);

  const toggleIsLiked = () => setIsLiked(!isLiked);

  return (
    <StyledMovieCard>
      <MovieCardPicture>
        <img
          src="https://images.moviesanywhere.com/45bc0ec075bfc0b4d8f184a7cc5bf993/876ed805-83b1-4387-b0d0-62d08c36536d.jpg"
          alt="Movie cover"
        />
        <MovieCardPictureOverlay>
          <MovieCardPictureOverlayText>
            <IconButton onClick={toggleIsLiked}>
              {isLiked ? <Favorite color="secondary" /> : <FavoriteBorder color="secondary" />}
            </IconButton>
            <Typography paragraph>9/10</Typography>
            <Typography paragraph>Action/Adventure</Typography>
          </MovieCardPictureOverlayText>
        </MovieCardPictureOverlay>
      </MovieCardPicture>
      <div>
        <Typography variant="h6">
          The Lord of the Rings: The Return of the King (2003)
        </Typography>
      </div>
    </StyledMovieCard>
  );
};

export default MovieCard;
