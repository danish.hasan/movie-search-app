import React from 'react';

import styled from 'styled-components';

import {
  AppBar,
  Toolbar,
  Typography,
  TextField,
  Button,
  IconButton,
  InputAdornment,
} from '@material-ui/core';

import { Search } from '@material-ui/icons/';

const StyledAppBar = styled(AppBar)`
  margin-bottom: 3rem;
`;

const StyledToolbar = styled(Toolbar)`
  justify-content: space-between;
  padding: 0.75rem;
`;

const StyledTextField = styled(TextField)`
  width: 50%;
`;

const StyledButton = styled(Button)`
  margin-right: 10px;
`;

const Header = () => (
  <StyledAppBar position="static">
    <StyledToolbar>
      <Typography variant="h5">Movie Search</Typography>
      <StyledTextField
        type="search"
        label="Search movies"
        color="secondary"
        variant="outlined"
        InputProps={{
          endAdornment: (
            <InputAdornment>
              <IconButton>
                <Search />
              </IconButton>
            </InputAdornment>
          ),
        }}
      />
      <div>
        <StyledButton variant="contained" color="secondary">Sign Up</StyledButton>
        <Button variant="contained" color="secondary">Login</Button>
      </div>
    </StyledToolbar>
  </StyledAppBar>
);

export default Header;
