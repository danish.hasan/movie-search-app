import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';

import { Avatar } from '@material-ui/core';

const PersonContainer = styled.div`
  margin-bottom: 1rem;
  `;

const StyledAvatar = styled(Avatar)`
  display: inline-block;
  margin-right: 0.5rem;
  vertical-align: middle;
`;

const Person = ({ name, pic }) => (
  <PersonContainer>
    <StyledAvatar alt={name} src={pic} />
    <span>{name}</span>
  </PersonContainer>
);

Person.propTypes = {
  name: PropTypes.string,
  pic: PropTypes.string,
};

Person.defaultProps = {
  name: 'John Doe',
  pic: 'https://img.yts.mx/assets/images/actors/thumb/nm0001392.jpg',
};

export default Person;
