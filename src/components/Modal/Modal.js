import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { Button, Dialog } from '@material-ui/core/';

const Modal = ({ children, label }) => {
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button variant="contained" color="secondary" onClick={handleClickOpen}>
        {label}
      </Button>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        {children}
      </Dialog>
    </div>
  );
};

Modal.propTypes = {
  children: PropTypes.node,
  label: PropTypes.string,
};

Modal.defaultProps = {
  children: null,
  label: '',
};

export default Modal;
