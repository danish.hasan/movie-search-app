import React from 'react';

import styled from 'styled-components';

import { MUI_PRIMARY, MUI_SECONDARY } from '../../utils/constants/colors';

const StyledFooter = styled.footer`
  align-items: center;
  background-color: ${MUI_PRIMARY};
  display: flex;
  flex-direction: row;
  font-size: 1.4rem;
  justify-content: center;
  padding: 1rem;

  & p {
    color: white;
  }
`;

const FooterNavigation = styled.ul`
  align-items: center;
  display: flex;
  justify-content: center;
  list-style: none;

  & li {
    margin-right: 1.5rem;
  }

  & a {
    color: ${MUI_SECONDARY};
    text-decoration: none;

  }
`;

const Footer = () => (
  <StyledFooter>
    <FooterNavigation>
      <li><a href="/">About Us</a></li>
      <li><a href="/">Careers</a></li>
      <li><a href="/">Contact</a></li>
    </FooterNavigation>
    <p>&copy;Movie Search App. All Rights Reserved</p>
  </StyledFooter>
);

export default Footer;
