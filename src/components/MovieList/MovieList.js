import React from 'react';
import PropTypes from 'prop-types';

import { Grid } from '@material-ui/core';

const MovieList = ({ children }) => (
  <Grid item container justifyContent="center">
    <Grid item xs={false} sm={8}>
      {children}
    </Grid>
  </Grid>
);

MovieList.propTypes = {
  children: PropTypes.node,
};

MovieList.defaultProps = {
  children: null,
};

export default MovieList;
