import React from 'react';

import { Typography } from '@material-ui/core';

import Person from '../Person';

const Review = () => (
  <div>
    <Person
      name="Reviewer #1"
      pic="https://img.yts.mx/assets/images/movies/The_Lord_of_the_Rings_The_Return_of_the_King_EXTENDED_2003/medium-screenshot2.jpg"
    />
    <Typography paragraph>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Mollitia laborum doloribus eaque rem,
      obcaecati placeat dolores laudantium sint enim cupiditate asperiores culpa sunt rerum unde
      quo in illo minima blanditiis!
    </Typography>
  </div>
);

export default Review;
