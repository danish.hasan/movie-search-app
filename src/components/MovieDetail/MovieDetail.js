import React from 'react';

import styled from 'styled-components';

import { Typography } from '@material-ui/core';

const RatingsIMDb = styled.div`
  & a {
    margin-right: 10px;
  }
`;

const RatingsRT = styled.div`
  & a {
    margin-right: 31px;
  }
`;

const MovieRatings = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  margin-bottom: 1rem;
`;

const MovieDetail = () => (
  <>
    <Typography variant="h4">The Lord of The Rings: The Return of the King (2003)</Typography>
    <Typography paragraph>Action/Adventure</Typography>
    <MovieRatings>
      <RatingsIMDb>
        <a href="https://www.imdb.com/title/tt0167260/">
          <img src="https://yts.mx/assets/images/website/logo-imdb.svg" alt="IMDb logo" />
        </a>
        <span>9/10</span>
      </RatingsIMDb>
      <RatingsRT>
        <a href="https://www.rottentomatoes.com/m/the_lord_of_the_rings_the_return_of_the_king">
          <img src="https://yts.mx/assets/images/website/rt-fresh.png" alt="RT logo" />
        </a>
        <span>93%</span>
      </RatingsRT>
    </MovieRatings>
    <div>
      <Typography paragraph>
        Gandalf and Aragorn lead the World of Men against
        Sauron&apos;s army to draw his gaze from Frodo
        and Sam as they approach Mount Doom with the One Ring.
      </Typography>
    </div>
  </>
);

export default MovieDetail;
