import React from 'react';
import PropTypes from 'prop-types';

import { Grid } from '@material-ui/core';

import Header from '../../components/Header';
import Footer from '../../components/Footer';

const StandardLayout = ({ children }) => (
  <Grid container direction="column">
    <Grid item>
      <Header />
    </Grid>
    <Grid item container justifyContent="center">
      <Grid item xs={false} sm={8}>
        {children}
      </Grid>
    </Grid>
    <Grid item>
      <Footer />
    </Grid>
  </Grid>
);

StandardLayout.propTypes = {
  children: PropTypes.node,
};

StandardLayout.defaultProps = {
  children: null,
};

export default StandardLayout;
