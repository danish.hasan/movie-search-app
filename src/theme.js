import { createTheme, responsiveFontSizes } from '@material-ui/core/styles';

import { MUI_PRIMARY, MUI_SECONDARY } from './utils/constants/colors';

const theme = responsiveFontSizes(createTheme({
  themeName: 'myTheme',
  palette: {
    primary: {
      main: MUI_PRIMARY,
    },
    secondary: {
      main: MUI_SECONDARY,
    },
  },
  typography: {
    fontFamily: [
      'Roboto',
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
  },
}));

export default theme;
